# TableWare Product Management System

This project implements a simple product management system for tableware products. It allows users to perform various operations such as retrieving all products, searching products by name, category, quantity, and price.

## Prerequisites

- Java Development Kit (JDK) 8 or higher
- An Integrated Development Environment (IDE) such as IntelliJ IDEA or Eclipse

## Getting Started

1. Clone the repository:
git clone https://gitlab.com/whomaun2/tableware.git
2. Open the project in your preferred IDE.

3. Build the project to resolve dependencies.

4. Run the `Main` class located in the `org.example` package. This will start the application.

## Usage

The application presents a menu to the user with the following options:

1. Get all products
2. Search product by name
3. Search product by category
4. Search product by quantity
5. Search product by price
0. Exit

To navigate the menu, enter the corresponding number and press Enter. Follow the on-screen prompts to provide additional input as necessary.

- Option 1 (`Get all products`) retrieves and displays all the tableware products available in the system.

- Option 2 (`Search product by name`) allows you to search for products by name. Enter the product name when prompted, and the system will display matching products.

- Option 3 (`Search product by category`) allows you to search for products by category. Enter the product category when prompted, and the system will display matching products.

- Option 4 (`Search product by quantity`) allows you to search for products by quantity. Enter the desired quantity when prompted, and the system will display products with that quantity.

- Option 5 (`Search product by price`) allows you to search for products by price. Enter the desired price when prompted, and the system will display products with that price.

- Option 0 (`Exit`) terminates the application.

## Data Source

The product data is stored in a CSV file located at `src/main/resources/Products.csv`. Make sure the file exists and contains valid product data in the expected format (ID, Name, Category, Quantity, Price).

You can modify the CSV file or provide your own file with product data as long as it follows the same structure.

## Contributing

Contributions are welcome! If you find any issues or have suggestions for improvements, please open an issue or submit a pull request.



