package org.example.DAO;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import org.example.Entity.Product;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DaoTableWare {

    public String csv = "Products.csv";
    private final String databaseUrl;

    public DaoTableWare() {
        this.databaseUrl = "jdbc:h2:file:./database";
    }

    // create tables if not exists
    public void initDatabase() {
        try(var connectionSource = getConnection()) {
            TableUtils.createTableIfNotExists(connectionSource, Product.class);
            initProducts(connectionSource);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void initProducts(ConnectionSource connectionSource) throws SQLException {
        Dao<Product, ?> dao = getProductDao(connectionSource);
        List<Product> csvFilePath = new ArrayList<>();
        if (dao.countOf() == 0) {
            try (BufferedReader bufferedReader = Files.newBufferedReader(Path.of(getClass().getClassLoader().getResource(csv).getFile()))) {
                String line;

                while ((line = bufferedReader.readLine()) != null) {
                    String[] data = line.split(",");
                    long id = Integer.parseInt(data[0]);
                    String name = data[1];
                    String category = data[2];
                    int quantity = Integer.parseInt(data[3]);
                    double price = Double.parseDouble(data[4]);

                    Product product = new Product(id, name, category, quantity, price);
                    csvFilePath.add(product);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            dao.create(csvFilePath);
        }
    }

    public static ProductDao getProductDao(ConnectionSource connectionSource) throws SQLException {
        return DaoManager.createDao(connectionSource, Product.class);
    }

    public ConnectionSource getConnection() throws SQLException {
        return new JdbcPooledConnectionSource(databaseUrl);
    }

    public List<Product> getAllProducts() throws SQLException {
        return getProductDao(getConnection()).queryForAll();
    }

    public List<Product> getProductByName(String name) throws SQLException {
        return getProductDao(getConnection()).findByName(name);
    }

    public List<Product> getProductByCategory(String category) throws SQLException {
        return getProductDao(getConnection()).findByCategory(category);
    }

    public List<Product> getProductByQuantity(int quantity) throws SQLException {
        return getProductDao(getConnection()).findByQuantity(quantity);
    }

    public List<Product> getProductByPrice(double price) throws SQLException {
        return getProductDao(getConnection()).findByPrice(price);
    }
}
