package org.example.DAO;

import com.j256.ormlite.dao.Dao;
import org.example.Entity.Product;

import java.sql.SQLException;
import java.util.List;

public interface ProductDao extends Dao<Product, Long> {
    List<Product> findByName(String name) throws SQLException;
    List<Product> findByCategory(String category) throws SQLException;
    List<Product> findByQuantity(int quantity) throws SQLException;
    List<Product> findByPrice(double price) throws SQLException;
}
