package org.example.DAO;


import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import org.example.Entity.Product;

import java.sql.SQLException;
import java.util.List;

public class ProductDaoImpl extends BaseDaoImpl<Product, Long>
        implements ProductDao {
    public ProductDaoImpl(JdbcPooledConnectionSource connectionSource) throws SQLException {
        super(connectionSource, Product.class);
    }

    @Override
    public List<Product> findByName(String name) throws SQLException {
        return super.query(super.queryBuilder().where().like("name", name).prepare());
    }

    @Override
    public List<Product> findByCategory(String category) throws SQLException {
        return super.query(super.queryBuilder().where().like("category", category).prepare());
    }

    @Override
    public List<Product> findByQuantity(int quantity) throws SQLException {
        return super.queryBuilder().where().eq("quantity", quantity).query();
    }

    @Override
    public List<Product> findByPrice(double price) throws SQLException {
        return super.queryBuilder().where().eq("price", price).query();
    }
}
