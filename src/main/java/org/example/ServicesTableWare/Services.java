package org.example.ServicesTableWare;

import org.example.Entity.Product;

import java.util.List;

public interface Services {
    List<Product> getAllProducts();
    Product SearchingByName(String name);
    List<Product> SearchingByCategory(String category);
    List<Product> SearchingByQuantity(int quantity);
    List<Product> SearchingByPrice(double price);
}
