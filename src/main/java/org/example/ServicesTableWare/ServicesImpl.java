package org.example.ServicesTableWare;

import org.example.DAO.DaoTableWare;
import org.example.Entity.Product;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;

public class ServicesImpl implements Services {
    private final DaoTableWare dao;

    public ServicesImpl() throws FileNotFoundException {
        this.dao = new DaoTableWare();
        this.dao.initDatabase();
    }

    @Override
    public List<Product> getAllProducts() {
        try {
            return dao.getAllProducts();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Product SearchingByName(String name) {
        try {
            return dao.getProductByName(name).get(0);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Product> SearchingByCategory(String category) {
        try {
            return dao.getProductByCategory(category);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Product> SearchingByQuantity(int quantity) {
        try {
            return dao.getProductByQuantity(quantity);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Product> SearchingByPrice(double price) {
        try {
            return dao.getProductByPrice(price);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
