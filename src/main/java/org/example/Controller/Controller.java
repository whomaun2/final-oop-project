package org.example.Controller;

import java.util.Scanner;

public interface Controller {
    void mainController();
    void getAll(Scanner scanner);
    void getName(Scanner scanner);
    void getCategory(Scanner scanner);
    void getQuantity(Scanner scanner);
    void getPrice(Scanner scanner);
}
