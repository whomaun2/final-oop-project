package org.example;

import org.example.Controller.ControllerImpl;

import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {

        ControllerImpl controller = new ControllerImpl();
        controller.mainController();

    }
}