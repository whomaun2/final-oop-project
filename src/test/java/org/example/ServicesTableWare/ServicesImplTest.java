package org.example.ServicesTableWare;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

class ServicesImplTest {

    public ServicesImpl services;

    @BeforeEach
    void setUp() throws FileNotFoundException {
        services = new ServicesImpl();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testGetAllProducts() {
        services.getAllProducts();
    }

    @Test
    void testSearchingByName() {
        String name = "Tray";
        var p = services.SearchingByName(name);
        assertEquals(14L, p.getId());
        assertEquals("Tray", p.getName());
    }

    @Test
    void testSearchingByCategory() {
        String category = "Tableware";
        var products = services.SearchingByCategory(category);
        assertEquals(20, products.size());
        assertEquals("Tableware", products.get(0).getCategory());
    }

    @Test
    void testSearchingByQuantity() {
        int quantity = 10;
        var products = services.SearchingByQuantity(quantity);
        assertEquals(3, products.size());
        assertEquals(10, products.get(0).getQuantity());
        assertEquals(10, products.get(1).getQuantity());
        assertEquals(10, products.get(2).getQuantity());
    }

    @Test
    void testSearchingByPrice() {
        double price = 29.0;
        var products = services.SearchingByPrice(price);
        assertEquals(1, products.size());
        assertEquals(29.0D, products.get(0).getPrice());
    }
}
